package com.sk89q.skmcl.swing;

import com.sk89q.skmcl.Launcher;
import com.sk89q.skmcl.application.Application;
import com.sk89q.skmcl.application.LatestStable;
import com.sk89q.skmcl.application.Version;
import com.sk89q.skmcl.minecraft.Minecraft;
import com.sk89q.skmcl.launch.*;
import static com.sk89q.skmcl.util.SharedLocale._;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.java.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.logging.Level;
import java.util.logging.Logger;


@Log
public class CreateSettingsDialog extends JDialog {
    @Getter
    public static String jvmargs;
    private final Launcher launcher;
    private Version version = new LatestStable();

    public CreateSettingsDialog(Window owner, @NonNull Launcher launcher) {
        super(owner, Dialog.ModalityType.DOCUMENT_MODAL);

        this.launcher = launcher;

        setTitle(_("createSettings.title"));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        initComponents();
        pack();
        setMinimumSize(new Dimension(350, 0));
        setResizable(false);
        setLocationRelativeTo(owner);
    }

    private void initComponents() {
        FormPanel form = new FormPanel();
        final JTextField jpathField = new JTextField();
        final JTextField jvmargsField = new JTextField();
        jpathField.setEnabled(false);
        jvmargsField.setEnabled(false);
        jpathField.setText(System.getProperty("java.home"));
        jvmargsField.setText("-Xms512M -Xmx1G");
        jvmargs = jvmargsField.getText(); // Вероятно, это не сработает при изменении JTextField и кнопки принять. Нужно исправить, если так и есть.
        LinedBoxPanel buttons = new LinedBoxPanel(true);
        JButton cancelButton = new JButton(_("button.cancel"));
        JButton createButton = new JButton(_("button.apply"));
        
        JCheckBox fullscrn = new JCheckBox(_("createSettigns.fullscrn"));
        JCheckBox offlinemode = new JCheckBox(_("createSettigns.offlinemode"));
        JCheckBox updatec = new JCheckBox(_("createSettings.updatec"));
        JCheckBox cleandir = new JCheckBox(_("createSettigns.cleandir"));
        JCheckBox extsettings = new JCheckBox(_("createSettings.extsettings"));

        form.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        form.addRow(fullscrn);
        form.addRow(offlinemode);
        form.addRow(updatec);
        form.addRow(cleandir);
        form.addRow(extsettings);
        form.addRow(new JLabel(_("createSettings.jpath")), jpathField);
        form.addRow(new JLabel(_("createSettings.jvmargs")), jvmargsField);
        
        buttons.addGlue();
        buttons.addElement(createButton);
        buttons.addElement(cancelButton);

        add(form, BorderLayout.CENTER);
        add(buttons, BorderLayout.SOUTH);

        getRootPane().setDefaultButton(createButton);

        cancelButton.addActionListener(ActionListeners.dispose(this));

        SwingHelper.focusLater(jpathField);
        
        // action listeners
         extsettings.addItemListener(new ItemListener(){
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        jpathField.setEnabled(true);
                        jvmargsField.setEnabled(true);
                    }
                    else if(e.getStateChange() == ItemEvent.DESELECTED){
                        jpathField.setEnabled(false);
                        jvmargsField.setEnabled(false);
                    }

                    validate();
                    repaint();
                }

           
            });
        
        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    saveSettingsDialog();
                } catch (Exception ex) {
                    Logger.getLogger(CreateSettingsDialog.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
    // actions
    private void saveSettingsDialog() throws Exception {
        throw new UnsupportedOperationException("Сохранение jpathfield + jvmargs / статуса чекбоксов fullscrn, extsettings + применение параметров чекбоксов (свитч на оффлайн режим и т.д.)"); 
    }
}
