package com.sk89q.skmcl.util;

public class LaunchConfig
{
	public static final String goToURLJButton = "http://pixelsky.ru/";                              // Ссылка в кнопке "Любая ссылка
	public static final String auth           = "https://authserver.mojang.com/authenticate";       // Ссылка до авторизации
        public static final String authRefresh    = "https://authserver.mojang.com/refresh";            //  ???
        public static final String skinsURL       = "http://skins.minecraft.net/MinecraftSkins/%s.png"; // Используется в com.sk89q.skmcl.minecraft.MinecraftFaceLoader
        public static final String libURL         = "https://s3.amazonaws.com/Minecraft.Download/libraries/";; //* Это всё нужно организовать всего лишь одной строкой (URL до папки со всем необходимым.)
        public static final String versionsURL    = "https://s3.amazonaws.com/Minecraft.Download/versions/versions.json"; //*
        public static final String libDir         = "https://s3.amazonaws.com/Minecraft.Download/libraries/"; //*
        public static final String verManifestURL = "https://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.json"; //*
        public static final String releaseMURL    = "http://s3.amazonaws.com/Minecraft.Download/versions/%s/%s.jar"; //*
        public static final String assetsURL      = "https://s3.amazonaws.com/MinecraftResources/"; //*

        
        
        
        
        public static final String launcherDir    = "pixelsky";
        public static final String launcherDirEx  = ".pixelsky";
    
}
